extern crate mioco;

use std::io::Result;
use std::io::Read;
use std::io::Write;
use std::net::SocketAddr;
use std::sync::Arc;
use mioco::sync::Mutex;
use mioco::tcp::Shutdown;
use mioco::tcp::TcpListener;
use mioco::tcp::TcpStream;

const CONTROL_ADDRESS: &'static str = "0.0.0.0:10158";
const ROBOT_ADDRESS: &'static str = "0.0.0.0:10157";

fn main() {
	mioco::start(|| -> Result<()> {
		let control_address = CONTROL_ADDRESS.parse::<SocketAddr>().unwrap();
		let robot_address = ROBOT_ADDRESS.parse::<SocketAddr>().unwrap();

		let control_listener = try!(TcpListener::bind(&control_address));
		let robot_listener = try!(TcpListener::bind(&robot_address));
		println!("Starting joist server, control on {:?} and robots on {:?}", try!(control_listener.local_addr()), try!(robot_listener.local_addr()));

		let robot_sockets: Arc<Mutex<Vec<TcpStream>>> = Arc::new(Mutex::new(Vec::new()));

		// Start new coroutine to wait for robot connections
		{
			let robot_sockets = robot_sockets.clone();
			mioco::spawn(move || -> Result<()> {
				loop {
					let robot_socket = match robot_listener.accept() {
						Err(e) => {
							println!("Error accepting robot connection: {}", e);
							continue;
						},
						Ok(sock) => sock,
					};
					println!("New robot connection from {}", robot_socket.peer_addr().unwrap());

					robot_sockets.lock().unwrap().push(robot_socket);
				}
			});
		}

		// Wait for new control connections
		loop {
			let mut control_socket = match control_listener.accept() {
				Err(e) => {
					println!("Error accepting control connection: {}", e);
					continue;
				},
				Ok(sock) => sock,
			};
			println!("New control connection from {}", control_socket.peer_addr().unwrap());

			// Start a new co-routine for the new connection.
			let robot_sockets = robot_sockets.clone();
			mioco::spawn(move || -> Result<()> {
				let mut buf = [0u8; 1024 * 16];
				loop {
					let size = try!(control_socket.read(&mut buf));
					if size == 0 {/* eof */ break; }
					let mut robot_sockets = robot_sockets.lock().unwrap();
					let mut dead_robots: Vec<usize> = Vec::new();
					for (index, robot_socket) in robot_sockets.iter_mut().enumerate() {
						if let Err(e) = robot_socket.write_all(&mut buf[0..size]) {
							println!("Error writing to robot socket: {}", e);
							if let Err(e) = robot_socket.shutdown(Shutdown::Both) {
								println!("Error closing dead robot socket: {}", e);
							}
							// Save it to remove from the list later
							dead_robots.push(index);
						}
					}
					// Remove dead robots
					for index in dead_robots.iter().rev() {
						robot_sockets.remove(*index);
					}
					println!("Wrote to {} robots, failed for {}", robot_sockets.len(), dead_robots.len());
				}

				Ok(())
			});
		}
	}).unwrap().unwrap();
}
